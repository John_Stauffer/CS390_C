#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 20


typedef struct Course {
    char *prefix;
    int number;
    char *title;
    int credits;
} COURSE;

typedef struct Major {
    char *majorName;
    int next;
    COURSE *course[MAX];
} MAJOR;



void printAllCourses(MAJOR *major);

COURSE *getCourse();

COURSE *searchCourse(MAJOR *major, char *courseName);

void addCourse(MAJOR *myMajor, COURSE *course);


COURSE *searchCourse(MAJOR *major, char *courseName);

void findCourse(MAJOR *major);

void addCourse(MAJOR *myMajor, COURSE *course);

char *getMajor() {
    printf("\n\tMajor");
    printf("\n\t------------------------------");
    char *text = (char *) malloc(sizeof(char) * 256);
    printf("\nMajor: ");
    fgets(text, 256, stdin);
    return text;
}

MAJOR *initMajor(char *majorName) {
    //Making majorName
    MAJOR *major;
    major = (MAJOR *) malloc(sizeof(MAJOR));
    int startingIndex = 0;
    for (int i = 0; i < MAX; i++) {
        major->course[i] = NULL;
    }
    major->majorName = majorName;
    major->next = startingIndex;
    return major;
}

void printCourse(COURSE *course) {
    if(course != NULL) {
        printf("Prefix:  %s", (*course).prefix);
        printf("Title:   %s", (*course).title);
        printf("Number:  %d\n", (*course).number);
        printf("Credits: %d\n", (*course).credits);
        printf("\n");
    }
}

void printAllCourses(MAJOR *major) {
    printf("\n\tAll Courses");
    printf("\n\t------------------------------");
    printf("\n\nMajor: %s\n", (*major).majorName);
    int records = (*major).next;
    for (int i = 0; i < records; i++) {
        printCourse((*major).course[i]);
    }
}

COURSE *getCourse() {
    COURSE *course = (struct Course *) malloc(sizeof(COURSE));

    char *prefixText, *titleText, *numberText, *creditsText; //allocate 256 bytes for each
    int number, credits;

    printf("\n\tAdd Course");
    printf("\n\t------------------------------");

    //Read in prefix string
    printf("\nEnter prefix: ");
    prefixText = malloc(sizeof(char) * 256);
    fgets(prefixText, 256, stdin); //reads up to 256 bytes
    course->prefix = prefixText; //dereference pointer

    //Read in title
    printf("Enter title: ");
    titleText = malloc(sizeof(char) * 256);
    fgets(titleText, 256, stdin);
    course->title = titleText;

    //Read in course number
    printf("Enter number: ");
    numberText = malloc(sizeof(char) * 256);
    fgets(numberText, 256, stdin);
    number = atoi(numberText);
    course->number = number;

    //Read in credits
    printf("Enter credits: ");
    creditsText = malloc(sizeof(char) * 256);
    fgets(creditsText, 256, stdin);
    credits = atoi(creditsText);
    course->credits = credits;

    printf("\n");

    return course;
}

COURSE *searchCourse(MAJOR *major, char *courseName) {
    int i = 0;
    COURSE **course;
    strtok(courseName, "\n");
    while ((*major).course[i] != NULL) {
        char *title = (*(*major).course[i]).title;
        strtok(title,"\n");
        if (title == courseName) {
            course = &(*major).course[i];
            return *course;
        }
        i++;
    }
    return NULL;
}

void addCourse(MAJOR *myMajor, COURSE *course);

void findCourse(MAJOR *major) {
    char *text = (char *) malloc(sizeof(char) * 256);
    printf("\n\tCourse");
    printf("\n\t------------------------------");
    printf("\nCourse Title: ");
    fgets(text, 256, stdin);
    printCourse(searchCourse(major, text));

}

void addCourse(MAJOR *myMajor, COURSE *course) {
    int index = (*myMajor).next;
    myMajor->course[index] = course;
    myMajor->next = index + 1;
}

void showMenu(MAJOR *major){
    char choice = '0';
    while (choice != '3') {
        printf("\n\tCourse Menu");
        printf("\n\t------------------------------");
        printf("\n\n\t 1. Add Course");
        printf("\n\t 2. Print All Courses");
        printf("\n\t 3. Find a Course");
        printf("\n\t 4. EXIT");
        printf("\n\n Enter Your Choice: ");
        choice = getchar();
        while (getchar() != '\n');
        switch (choice) {
            case '1':
                addCourse(major, getCourse());
                break;
            case '2':
                printAllCourses(major);
                break;
            case '3':
                findCourse(major);
                break;
            case '4':
                printf("\n\nEXIT \n");
                break;
            default:
                printf("\n\nINVALID SELECTION...Please try again\n");
        }
    }

}

int main(void) {
    MAJOR *major;
    char *majorName = getMajor();
    major = initMajor(majorName);
    showMenu(major);
    free(major);
    return 0;
}


